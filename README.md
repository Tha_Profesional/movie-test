# How to run it
For the API:
- Open the API solution in visual studio, then build it, then run it using IIS express

If you want to run it some other way, no stress. Please chuck the API URL in:
	~/movie-ui/src/environments/environments.ts

For the UI:
- In the UI directory, run "yarn install" to install the node modules (npm install might work too)
- Run "ng serve"


# Assumptions
- Looking at the data I've assumed the ID's are the same between sources, but each has their own prefix. This made the mapping and front end much more simple. In a work-environment this is something I'd definitely chase up.
- I also assumed it's fine to return the list with one call, then comparisons on demand


# Notes
- The front and back end should be in different git repositories, but I left them together for ease of submission.
- I'd have much rather used dotnet core and autofac, but in the interests of time I stuck with technology I'm familiar with.
- On the same lines, I'm not very familiar with modern Angular (in my current role I work with AngularJS) but picked to work with it for Angular CLI - which made setting up a new project trivial.
- On the note of time, I ran out of time to host it - sorry :(
- I also ran out of time to finish my unit tests (which hurt me a great deal). In the back end I wrote most of them, and put in comments to outline the ones I couldn't write. In the front end I wrote one test (for movie-list.component) then also just outlined the tests I didn't write.
- On that note, the front end tests don't actually pass :(


# Next steps (if I had more time)
- Finish unit tests
- Get the front end tests to actually pass
- Host the site
- Send the log messages somewhere
- Apologise to any designers I run into on the street