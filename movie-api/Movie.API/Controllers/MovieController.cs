﻿using Movie.Models;
using Movie.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Movie.API.Controllers
{
    public class MovieController : ApiController
    {
        private readonly IGetMovieList _getMovieList;
        private readonly IGetMoviePrices _getMoviePrices;

        public MovieController(
            IGetMovieList getMovieList,
            IGetMoviePrices getMoviePrices)
        {
            _getMovieList = getMovieList;
            _getMoviePrices = getMoviePrices;
        }

        [HttpGet]
        [Route("movies")]
        public Task<IList<MovieSummary>> GetMovieList()
        {
            return _getMovieList.ExecAsync();
        }

        [HttpGet]
        [Route("movie/{movieId}/prices")]
        public Task<IList<MoviePrice>> GetMoviePriceComparison(string movieId)
        {
            return _getMoviePrices.ExecAsync(movieId);
        }
    }
}
