﻿using FluentAssertions;
using Movie.Models;
using Movie.Models.Constants;
using Movie.Models.DTOs;
using Movie.Models.Enums;
using Movie.Services.Mappers;
using Movie.Services.Proxies;
using NSubstitute;
using System.Collections.Generic;
using Xunit;

namespace Movie.Services.Tests
{
    public class GetMoviePricesTests
    {
        private readonly IMapMoviePrices _mapMoviePricesMock;
        private readonly IWebjetAPIProxy _webjetAPIProxyMock;

        private readonly GetMoviePrices _getMoviePrices;

        public GetMoviePricesTests()
        {
            _mapMoviePricesMock = Substitute.For<IMapMoviePrices>();
            _webjetAPIProxyMock = Substitute.For<IWebjetAPIProxy>();

            _getMoviePrices = new GetMoviePrices(
                _mapMoviePricesMock,
                _webjetAPIProxyMock);
        }

        [Fact]
        public async void ExecAsync_EverythignNormal_CallsProxyForAllProviders()
        {
            // arrange
            var movieId = "Captain Bildad";

            var expectedCinemaWorldUrl = $"/api/{ProviderConstants.CINEMAWORLD_URL_SEGMENT}/movie/{ProviderConstants.CINEMAWORLD_ID_PREFIX}{movieId}";
            var expectedFilmWorldUrl = $"/api/{ProviderConstants.FILMWORLD_URL_SEGMENT}/movie/{ProviderConstants.FILMWORLD_ID_PREFIX}{movieId}";

            // act
            await _getMoviePrices.ExecAsync(movieId);

            // assert
            await _webjetAPIProxyMock.ReceivedWithAnyArgs(2).GetAsync<MovieDTO>(Arg.Any<string>());
            await _webjetAPIProxyMock.Received().GetAsync<MovieDTO>(expectedCinemaWorldUrl);
            await _webjetAPIProxyMock.Received().GetAsync<MovieDTO>(expectedFilmWorldUrl);
        }

        [Fact]
        public async void ExecAsync_EverythignNormal_ReturnsMappedMoviePrices()
        {
            // arrange
            var movieId = "Captain Peleg";

            _webjetAPIProxyMock
                .GetAsync<MovieDTO>(Arg.Any<string>())
                .Returns(new MovieDTO());

            var expected = new List<MoviePrice>
            {
                new MoviePrice()
            };

            _mapMoviePricesMock
                .Exec(Arg.Any<Dictionary<Providers, MovieDTO>>())
                .Returns(expected);

            // act
            var moviePrices = await _getMoviePrices.ExecAsync(movieId);

            // assert
            moviePrices.Should().BeEquivalentTo(expected);
        }
    }
}
