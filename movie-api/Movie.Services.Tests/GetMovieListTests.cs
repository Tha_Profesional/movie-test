﻿using FluentAssertions;
using Movie.Models;
using Movie.Models.Constants;
using Movie.Models.DTOs;
using Movie.Services.Mappers;
using Movie.Services.Proxies;
using NSubstitute;
using System.Collections.Generic;
using Xunit;

namespace Movie.Services.Tests
{
    public class GetMovieListTests
    {
        private readonly IMapMovieSummaryList _mapMovieSummaryListMock;
        private readonly IWebjetAPIProxy _webjetAPIProxyMock;

        private readonly GetMovieList _getMovieList;

        public GetMovieListTests()
        {
            _mapMovieSummaryListMock = Substitute.For<IMapMovieSummaryList>();
            _webjetAPIProxyMock = Substitute.For<IWebjetAPIProxy>();

            _getMovieList = new GetMovieList(
                _mapMovieSummaryListMock,
                _webjetAPIProxyMock);
        }

        [Fact]
        public async void ExecAsync_EverythignNormal_CallsProxyForAllProviders()
        {
            // arrange
            var expectedCinemaWorldUrl = $"/api/{ProviderConstants.CINEMAWORLD_URL_SEGMENT}/movies";
            var expectedFilmWorldUrl = $"/api/{ProviderConstants.FILMWORLD_URL_SEGMENT}/movies";

            // act
            await _getMovieList.ExecAsync();

            // assert
            await _webjetAPIProxyMock.ReceivedWithAnyArgs(2).GetAsync<MovieSummaryListDTO>(Arg.Any<string>());
            await _webjetAPIProxyMock.Received().GetAsync<MovieSummaryListDTO>(expectedCinemaWorldUrl);
            await _webjetAPIProxyMock.Received().GetAsync<MovieSummaryListDTO>(expectedFilmWorldUrl);
        }

        [Fact]
        public async void ExecAsync_EverythignNormal_ReturnsMappedMovieSummaryLists()
        {
            // arrange
            _webjetAPIProxyMock
                .GetAsync<MovieSummaryListDTO>(Arg.Any<string>())
                .Returns(new MovieSummaryListDTO());

            var expected = new List<MovieSummary>
            {
                new MovieSummary()
            };

            _mapMovieSummaryListMock
                .Exec(Arg.Any<IList<MovieSummaryListDTO>>())
                .Returns(expected);

            // act
            var movieList = await _getMovieList.ExecAsync();

            // assert
            movieList.Should().BeEquivalentTo(expected);
        }
    }
}
