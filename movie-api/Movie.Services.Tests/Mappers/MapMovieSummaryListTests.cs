﻿using FluentAssertions;
using Movie.Models;
using Movie.Models.DTOs;
using Movie.Services.Mappers;
using NSubstitute;
using System.Collections.Generic;
using Xunit;

namespace Movie.Services.Tests.Mappers
{
    public class MapMovieSummaryListTests
    {
        private readonly IMapMovieSummary _mapMovieSummaryMock;

        private readonly MapMovieSummaryList _mapMovieSummaryList;

        public MapMovieSummaryListTests()
        {
            _mapMovieSummaryMock = Substitute.For<IMapMovieSummary>();

            _mapMovieSummaryList = new MapMovieSummaryList(_mapMovieSummaryMock);
        }

        [Fact]
        public void Exec_NullMovies_DoesntCrash()
        {
            // arrange
            var dtoLists = new List<MovieSummaryListDTO>
            {
                new MovieSummaryListDTO(),
                null
            };

            // act
            // assert
            _mapMovieSummaryList.Exec(dtoLists);
        }

        [Fact]
        public void Exec_DuplicateMovieIds_FilteredOut()
        {
            // arrange
            _mapMovieSummaryMock.Exec(Arg.Any<MovieSummaryDTO>()).Returns(args => new MovieSummary { Id = ((MovieSummaryDTO)args[0]).Id });

            var firstMovieId = "Starbuck";
            var firstMovieDto = new MovieSummaryDTO { Id = firstMovieId };

            var secondMovieId = "Stubb";
            var secondMovieDto = new MovieSummaryDTO { Id = secondMovieId };

            var duplicateMovieDtp = new MovieSummaryDTO { Id = firstMovieId };


            var dtoLists = new List<MovieSummaryListDTO>
            {
                new MovieSummaryListDTO { Movies = new List<MovieSummaryDTO> { firstMovieDto } },
                new MovieSummaryListDTO { Movies = new List<MovieSummaryDTO> { secondMovieDto, duplicateMovieDtp } }
            };

            // act
            var movieSummaries = _mapMovieSummaryList.Exec(dtoLists);

            // assert
            movieSummaries.Should().HaveCount(2);
            movieSummaries.Should().Contain(movieSummary => movieSummary.Id == firstMovieId);
            movieSummaries.Should().Contain(movieSummary => movieSummary.Id == secondMovieId);
        }
    }
}
