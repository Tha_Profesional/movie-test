﻿using FluentAssertions;
using Movie.Models.Constants;
using Movie.Models.DTOs;
using Movie.Services.Mappers;
using Xunit;

namespace Movie.Services.Tests.Mappers
{
    public class MapMovieSummaryTests
    {
        private readonly MapMovieSummary _mapMovieSummary;

        public MapMovieSummaryTests()
        {
            _mapMovieSummary = new MapMovieSummary();
        }

        [Fact]
        public void Exec_ValidSummary_ReturnsMappedFields()
        {
            // arrange
            var dto = new MovieSummaryDTO
            {
                Id = "Queequeg",
                Poster = "Tashtego",
                Title = "Daggoo",
                Year = "Fedallah"
            };

            // act
            var movieSummary = _mapMovieSummary.Exec(dto);

            // assert
            movieSummary.Id.Should().Be(dto.Id);
            movieSummary.Poster.Should().Be(dto.Poster);
            movieSummary.Title.Should().Be(dto.Title);
            movieSummary.Year.Should().Be(dto.Year);
        }

        [Theory]
        [InlineData(ProviderConstants.CINEMAWORLD_ID_PREFIX)]
        [InlineData(ProviderConstants.FILMWORLD_ID_PREFIX)]
        public void Exec_IdStartsWithPrefixToTrim_PrefixTrimmed(string prefix)
        {
            // arrange
            var expected = "Pip";

            var dto = new MovieSummaryDTO
            {
                Id = $"{prefix}{expected}"
            };

            // act
            var movieSummary = _mapMovieSummary.Exec(dto);

            // assert
            movieSummary.Id.Should().Be(expected);
        }
    }
}
