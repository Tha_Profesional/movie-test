﻿using FluentAssertions;
using Movie.Models;
using Movie.Models.Constants;
using Movie.Models.DTOs;
using Movie.Models.Enums;
using Movie.Services.Mappers;
using System.Collections.Generic;
using Xunit;

namespace Movie.Services.Tests.Mappers
{
    public class MapMoviePricesTests
    {
        private readonly MapMoviePrices _mapMoviePrices;

        public MapMoviePricesTests()
        {
            _mapMoviePrices = new MapMoviePrices();
        }

        [Fact]
        public void Exec_NoValues_ReturnsNull()
        {
            // arrange
            var providerDtoDictionary = new Dictionary<Providers, MovieDTO>
            {
                { Providers.Cinemaworld, null },
                { Providers.Filmworld, null }
            };

            // act
            var moviePriceList = _mapMoviePrices.Exec(providerDtoDictionary);

            // assert
            moviePriceList.Should().BeNull();
        }

        [Fact]
        public void Exec_ValidValues_ReturnsMappedPrices()
        {
            // arrange
            var providerDtoDictionary = new Dictionary<Providers, MovieDTO>
            {
                { Providers.Cinemaworld, new MovieDTO { Price = "10" } },
                { Providers.Filmworld, new MovieDTO { Price = "20" } },
            };

            var expected = new List<MoviePrice>
            {
                new MoviePrice { Provider = ProviderConstants.CINEMAWORLD_DISPLAY_NAME, Price = "10" },
                new MoviePrice { Provider = ProviderConstants.FILMWORLD_DISPLAY_NAME, Price = "20" }
            };

            // act
            var moviePriceList = _mapMoviePrices.Exec(providerDtoDictionary);

            // assert
            moviePriceList.Should().BeEquivalentTo(expected);
        }
    }
}
