﻿using System.Collections.Generic;

namespace Movie.Models.Constants
{
    public static class ProviderConstants
    {
        public const string CINEMAWORLD_DISPLAY_NAME = "CinemaWorld";
        public const string FILMWORLD_DISPLAY_NAME = "FilmWorld";

        public const string CINEMAWORLD_URL_SEGMENT = "cinemaworld";
        public const string FILMWORLD_URL_SEGMENT = "filmworld";

        public const string CINEMAWORLD_ID_PREFIX = "cw";
        public const string FILMWORLD_ID_PREFIX = "fw";

        public static readonly IList<string> ID_PREFIXES = new List<string>
        {
            CINEMAWORLD_ID_PREFIX,
            FILMWORLD_ID_PREFIX
        };
    }
}
