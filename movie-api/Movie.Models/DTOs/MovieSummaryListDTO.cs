﻿using System.Collections.Generic;

namespace Movie.Models.DTOs
{
    public class MovieSummaryListDTO
    {
        public IList<MovieSummaryDTO> Movies { get; set; }
    }
}
