﻿using System.Collections.Generic;

namespace Movie.Models
{
    public class MovieSummary
    {
        public string Id { get; set; }
        public string Poster { get; set; }
        public string Title { get; set; }
        public string Year { get; set; }
    }
}
