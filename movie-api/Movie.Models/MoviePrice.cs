﻿namespace Movie.Models
{
    public class MoviePrice
    {
        public string Provider { get; set; }
        public string Price { get; set; }
    }
}
