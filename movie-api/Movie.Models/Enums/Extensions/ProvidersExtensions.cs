﻿using Movie.Models.Constants;

namespace Movie.Models.Enums.Extensions
{
    public static class ProvidersExtensions
    {
        public static string GetDisplayName(this Providers provider)
        {
            switch (provider)
            {
                case Providers.Cinemaworld:
                    return ProviderConstants.CINEMAWORLD_DISPLAY_NAME;
                case Providers.Filmworld:
                    return ProviderConstants.FILMWORLD_DISPLAY_NAME;
                default:
                    return string.Empty;
            }
        }

        public static string GetUrlSegment(this Providers provider)
        {
            switch (provider)
            {
                case Providers.Cinemaworld:
                    return ProviderConstants.CINEMAWORLD_URL_SEGMENT;
                case Providers.Filmworld:
                    return ProviderConstants.FILMWORLD_URL_SEGMENT;
                default:
                    return string.Empty;
            }
        }

        public static string GetIdPrefix(this Providers provider)
        {
            switch (provider)
            {
                case Providers.Cinemaworld:
                    return ProviderConstants.CINEMAWORLD_ID_PREFIX;
                case Providers.Filmworld:
                    return ProviderConstants.FILMWORLD_ID_PREFIX;
                default:
                    return string.Empty;
            }
        }
    }
}
