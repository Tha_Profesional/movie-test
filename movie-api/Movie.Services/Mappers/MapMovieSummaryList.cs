﻿using Movie.Models;
using Movie.Models.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace Movie.Services.Mappers
{
    internal class MapMovieSummaryList : IMapMovieSummaryList
    {
        private readonly IMapMovieSummary _mapMovieSummary;

        public MapMovieSummaryList(IMapMovieSummary mapMovieSummary)
        {
            _mapMovieSummary = mapMovieSummary;
        }

        public IList<MovieSummary> Exec(IList<MovieSummaryListDTO> dtoLists)
        {
            return dtoLists
                .Where(dtoList => dtoList?.Movies != null)
                .SelectMany(dtoList => dtoList.Movies)
                .Select(_mapMovieSummary.Exec)
                .GroupBy(movieSummary => movieSummary.Id)
                .Select(movieSummaries => movieSummaries.First())
                .ToList();
        }
    }
}
