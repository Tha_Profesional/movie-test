﻿using Movie.Models;
using Movie.Models.DTOs;
using Movie.Models.Enums;
using Movie.Models.Enums.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Movie.Services.Mappers
{
    internal class MapMoviePrices : IMapMoviePrices
    {
        public IList<MoviePrice> Exec(Dictionary<Providers, MovieDTO> providerDtoDictionary)
        {
            providerDtoDictionary = providerDtoDictionary
                .Where(providerDto => providerDto.Value != null)
                .ToDictionary(providerDto => providerDto.Key, providerDto => providerDto.Value);

            if (providerDtoDictionary.Count == 0)
            {
                return null;
            }

            return providerDtoDictionary
                .Select(providerMovie => new MoviePrice
                {
                    Provider = providerMovie.Key.GetDisplayName(),
                    Price = providerMovie.Value.Price
                })
                .ToList();
        }
    }
}
