﻿using Movie.Models;
using Movie.Models.DTOs;

namespace Movie.Services.Mappers
{
    public interface IMapMovieSummary
    {
        MovieSummary Exec(MovieSummaryDTO dtoList);
    }
}
