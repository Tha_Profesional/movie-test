﻿using Movie.Models;
using Movie.Models.DTOs;
using Movie.Models.Enums;
using System.Collections.Generic;

namespace Movie.Services.Mappers
{
    public interface IMapMoviePrices
    {
        IList<MoviePrice> Exec(Dictionary<Providers, MovieDTO> providerMovieDictionary);
    }
}
