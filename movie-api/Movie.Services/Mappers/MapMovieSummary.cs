﻿using Movie.Models;
using Movie.Models.Constants;
using Movie.Models.DTOs;

namespace Movie.Services.Mappers
{
    internal class MapMovieSummary : IMapMovieSummary
    {
        public MovieSummary Exec(MovieSummaryDTO dto)
        {
            return new MovieSummary // OR use automapper instead
            {
                Id = TrimIdPrefix(dto.Id),
                Poster = dto.Poster,
                Title = dto.Title,
                Year = dto.Year
            };
        }

        private string TrimIdPrefix(string id)
        {
            foreach (var prefix in ProviderConstants.ID_PREFIXES)
            {
                if (id.StartsWith(prefix))
                {
                    return id.TrimStart(prefix.ToCharArray());
                }
            }

            return id;
        }
    }
}
