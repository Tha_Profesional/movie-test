﻿using Movie.Models;
using Movie.Models.DTOs;
using System.Collections.Generic;

namespace Movie.Services.Mappers
{
    public interface IMapMovieSummaryList
    {
        IList<MovieSummary> Exec(IList<MovieSummaryListDTO> dtoLists);
    }
}
