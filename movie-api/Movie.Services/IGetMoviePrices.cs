﻿using Movie.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Movie.Services
{
    public interface IGetMoviePrices
    {
        Task<IList<MoviePrice>> ExecAsync(string movieId);
    }
}
