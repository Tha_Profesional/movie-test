﻿using log4net;
using Movie.Services.Mappers;
using Movie.Services.Proxies;
using Ninject.Modules;
using System;
using System.Configuration;
using System.Net.Http;

namespace Movie.Services.Ioc
{
    public class MovieServicesBindingModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILog>().ToMethod(ctx => LogManager.GetLogger(ctx.Request.Target.Member.DeclaringType));

            var httpClient = new HttpClient();
            httpClient.Timeout = TimeSpan.FromMilliseconds(1000);
            Bind<HttpClient>().ToConstant(httpClient).InSingletonScope();

            Bind<IGetMovieList>().To<GetMovieList>().InSingletonScope();
            Bind<IGetMoviePrices>().To<GetMoviePrices>().InSingletonScope();

            // mappers
            Bind<IMapMoviePrices>().To<MapMoviePrices>().InSingletonScope();
            Bind<IMapMovieSummary>().To<MapMovieSummary>().InSingletonScope();
            Bind<IMapMovieSummaryList>().To<MapMovieSummaryList>().InSingletonScope();

            var maxRetriesString = ConfigurationManager.AppSettings["api.max.retries"];
            if (!int.TryParse(maxRetriesString, out int maxRetries))
            {
                maxRetries = 2;
            }

            // proxy
            Bind<IWebjetAPIProxy>().To<WebjetAPIProxy>().InSingletonScope()
                .WithConstructorArgument("accessToken", ConfigurationManager.AppSettings["webjet.api.key"]) // TODO: move into web.config
                .WithConstructorArgument("baseUrl", ConfigurationManager.AppSettings["webjet.base.url"])
                .WithConstructorArgument("maxRetries", maxRetries);
        }
    }
}
