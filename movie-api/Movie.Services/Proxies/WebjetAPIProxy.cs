﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Movie.Services.Proxies
{
    internal class WebjetAPIProxy : IWebjetAPIProxy
    {
        private readonly ILog _log;

        private readonly HttpClient _httpClient;

        private readonly string _accessToken;
        private readonly string _baseUrl;
        private readonly int _maxRetries;

        public WebjetAPIProxy(
            ILog log,
            HttpClient httpClient,
            string accessToken,
            string baseUrl,
            int maxRetries)
        {
            _log = log;
            _httpClient = httpClient;
            _accessToken = accessToken;
            _baseUrl = baseUrl;
            _maxRetries = maxRetries;
        }

        public async Task<T> GetAsync<T>(string relativeUrl)
        {
            var url = $"{_baseUrl}/{relativeUrl}";

            string content = null;
            var retries = 0;

            while (content == null)
            {
                try
                {
                    var request = new HttpRequestMessage(HttpMethod.Get, url);
                    request.Headers.Add("x-access-token", _accessToken);

                    var response = await _httpClient.SendAsync(request);

                    if (!response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.NotFound)
                    {
                        _log.Warn($"error: status code {response.StatusCode} recieved when calling {relativeUrl}");
                        retries++;
                    }
                    else
                    {
                        content = await response.Content.ReadAsStringAsync();
                    }
                }
                catch (Exception e)
                {
                    _log.Warn($"error: exception when calling {relativeUrl}", e);
                    retries++;
                }

                if (retries >= _maxRetries)
                {
                    _log.Error($"error: failed to call {relativeUrl} {retries} times");
                    return default(T);
                }
            }

            return JsonConvert.DeserializeObject<T>(content);
        }
    }
}
