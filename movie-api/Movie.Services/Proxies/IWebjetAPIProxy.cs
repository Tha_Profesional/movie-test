﻿using System.Threading.Tasks;

namespace Movie.Services.Proxies
{
    public interface IWebjetAPIProxy
    {
        Task<T> GetAsync<T>(string relativeUrl);
    }
}
