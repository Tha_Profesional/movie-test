﻿using Movie.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Movie.Services
{
    public interface IGetMovieList
    {
        Task<IList<MovieSummary>> ExecAsync();
    }
}
