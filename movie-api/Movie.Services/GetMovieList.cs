﻿using Movie.Models;
using Movie.Models.DTOs;
using Movie.Models.Enums;
using Movie.Models.Enums.Extensions;
using Movie.Services.Mappers;
using Movie.Services.Proxies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movie.Services
{
    internal class GetMovieList : IGetMovieList
    {
        private readonly IMapMovieSummaryList _mapMovieSummaryList;
        private readonly IWebjetAPIProxy _webjetAPIProxy;

        public GetMovieList(
            IMapMovieSummaryList mapMovieSummaryList,
            IWebjetAPIProxy webjetAPIProxy)
        {
            _mapMovieSummaryList = mapMovieSummaryList;
            _webjetAPIProxy = webjetAPIProxy;
        }

        public async Task<IList<MovieSummary>> ExecAsync()
        {
            var providerTasks = Enum.GetValues(typeof(Providers))
                .Cast<Providers>()
                .Select(CreateMovieSummaryTaskForProvider)
                .ToList();

            await Task.WhenAll(providerTasks);

            var movieSummaryLists = providerTasks
                .Select(task => task.Result)
                .ToList();

            return _mapMovieSummaryList.Exec(movieSummaryLists);
        }

        private Task<MovieSummaryListDTO> CreateMovieSummaryTaskForProvider(Providers provider)
        {
            var url = $"/api/{provider.GetUrlSegment()}/movies";
            return _webjetAPIProxy.GetAsync<MovieSummaryListDTO>(url);
        }
    }
}
