﻿using Movie.Models;
using Movie.Models.DTOs;
using Movie.Models.Enums;
using Movie.Models.Enums.Extensions;
using Movie.Services.Mappers;
using Movie.Services.Proxies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movie.Services
{
    public class GetMoviePrices : IGetMoviePrices
    {
        private readonly IMapMoviePrices _mapMoviePrices;
        private readonly IWebjetAPIProxy _webjetAPIProxy;

        public GetMoviePrices(
            IMapMoviePrices mapMoviePrices,
            IWebjetAPIProxy webjetAPIProxy)
        {
            _mapMoviePrices = mapMoviePrices;
            _webjetAPIProxy = webjetAPIProxy;
        }

        public async Task<IList<MoviePrice>> ExecAsync(string movieId)
        {
            var providerTaskDictionary = Enum.GetValues(typeof(Providers))
                .Cast<Providers>()
                .ToDictionary(provider => provider, provider => CreateMovieTaskForProvider(provider, movieId));

            var providerTasks = providerTaskDictionary.Values.ToList();

            await Task.WhenAll(providerTasks);

            var providerMovieDictionary = providerTaskDictionary
                .ToDictionary(providerTask => providerTask.Key, providerTask => providerTask.Value.Result);

            return _mapMoviePrices.Exec(providerMovieDictionary);
        }

        private Task<MovieDTO> CreateMovieTaskForProvider(Providers provider, string movieId)
        {
            var url = $"/api/{provider.GetUrlSegment()}/movie/{provider.GetIdPrefix()}{movieId}";
            return _webjetAPIProxy.GetAsync<MovieDTO>(url);
        }
    }
}
