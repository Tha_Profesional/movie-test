export class MovieSummary {
  Id: string;
  Poster: string;
  Title: string;
  Year: string;
}
