import { Component, OnInit } from '@angular/core';
import { MovieListService } from './services/movie-list.service';
import { MovieSummary } from './models/movie-summary';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {
  hasError: boolean;
  hasNoMovies: boolean;
  isLoading: boolean;
  movieSummaries: [MovieSummary];

  constructor(private movieListService: MovieListService) { }

  ngOnInit() {
    this.getMovieList();
  }

  getMovieList() {
    this.hasError = false;
    this.hasNoMovies = false;
    this.isLoading = true;

    this.movieListService.getMovieList()
      .subscribe(
        data => {
          this.movieSummaries = data;

          if (this.movieSummaries.length <= 0) {
            this.hasNoMovies = true;
          }
        },
        () => {
          this.hasError = true;
        },
        () => this.isLoading = false
      );
  }
}
