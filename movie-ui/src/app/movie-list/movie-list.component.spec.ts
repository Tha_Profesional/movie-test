import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieListComponent } from './movie-list.component';

describe('MovieListComponent', () => {
  let component: MovieListComponent;
  let fixture: ComponentFixture<MovieListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should get movie list', () => {
      // arrange
      component.getMovieList = jasmine.createSpy('getMovieList');

      // act
      component.ngOnInit();

      // assert
      expect(component.getMovieList).toHaveBeenCalled();
    });
  });

  // describe('getMovieList')
    // it('should reset state')

    // it('should request the movies')

    // describe('get movie list')
      // describe('success')
        // it('should grab a copy of the list')

        // it(`should flag if there's no movies`)

        // it('should stop loading')
      // describe('error')
        // it('should flag error')

        // it('should stop loading')

});
