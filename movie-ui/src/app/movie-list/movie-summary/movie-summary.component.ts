import { Component, OnInit, Input } from '@angular/core';
import { MovieSummary } from '../models/movie-summary';

@Component({
  selector: 'app-movie-summary',
  templateUrl: './movie-summary.component.html',
  styleUrls: ['./movie-summary.component.scss']
})
export class MovieSummaryComponent implements OnInit {
  displayComparison: boolean;

  @Input()
  movieSummary: MovieSummary;

  constructor() { }

  ngOnInit() { }

  comparePrices() {
    this.displayComparison = true;
  }

}
