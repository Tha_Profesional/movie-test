import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviePriceComparisonComponent } from './movie-price-comparison.component';

describe('MoviePriceComparisonComponent', () => {
  let component: MoviePriceComparisonComponent;
  let fixture: ComponentFixture<MoviePriceComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoviePriceComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviePriceComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // describe('ngOnInit')
    // it('should grab the price comparison')

  // describe('getMoviePriceComparison')
    // it('should reset state')

    // it('should request the movie price comparison')

    // describe('get movie price comparison')
      // describe('success')
        // it('should grab a copy of prices')

        // it(`should flag if there's no prices`)

        // it('should stop loading')
      // describe('error')
        // it('should flag error')

        // it('should stop loading')
});
