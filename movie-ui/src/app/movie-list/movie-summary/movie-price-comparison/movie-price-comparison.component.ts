import { Component, OnInit, Input } from '@angular/core';
import { MoviePrice } from './models/movie-price';
import { MoviePriceComparisonService } from './services/movie-price-comparison.service';

@Component({
  selector: 'app-movie-price-comparison',
  templateUrl: './movie-price-comparison.component.html',
  styleUrls: ['./movie-price-comparison.component.scss']
})
export class MoviePriceComparisonComponent implements OnInit {
  hasError: boolean;
  hasNoPrice: boolean;
  isLoading: boolean;
  moviePrices: [MoviePrice];

  @Input()
  movieId: string;

  constructor(private moviePriceComparisonService: MoviePriceComparisonService) { }

  ngOnInit() {
    this.getMoviePriceComparison();
  }

  getMoviePriceComparison() {
    this.hasError = false;
    this.hasNoPrice = false;
    this.isLoading = true;

    this.moviePriceComparisonService.getPriceComparison(this.movieId)
      .subscribe(
        data => {
          this.moviePrices = data;

          if (this.moviePrices.length <= 0) {
            this.hasNoPrice = true;
          }
        },
        () => {
          this.hasError = true;
        },
        () => this.isLoading = false
      );
  }

  getPriceDisplay(price) {
    return parseFloat(price).toFixed(2);
  }

}
