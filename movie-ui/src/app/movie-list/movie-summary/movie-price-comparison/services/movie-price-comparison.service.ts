import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MoviePrice } from '../models/movie-price';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MoviePriceComparisonService {

  constructor(private httpClient: HttpClient) { }

  getPriceComparison(movieId: string) {
    return this.httpClient.get<[MoviePrice]>(`${environment.apiUrl}/movie/${movieId}/prices`);
  }
}
