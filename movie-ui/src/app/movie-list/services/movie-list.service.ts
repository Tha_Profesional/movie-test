import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MovieSummary } from '../models/movie-summary';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MovieListService {

  constructor(private httpClient: HttpClient) { }

  getMovieList() {
    return this.httpClient.get<[MovieSummary]>(`${environment.apiUrl}/movies`); // TODO: don't hardcode url
  }
}
